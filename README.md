![esai-pomodoro logo - a tomato](https://esai-pomodoro.netlify.com/img/icons/android-chrome-192x192.png)

# esai-pomodoro

[![pipeline status](https://gitlab.com/esai256/esai-pomodoro/badges/master/pipeline.svg)](https://gitlab.com/esai256/esai-pomodoro/commits/master)

[![Netlify Status](https://api.netlify.com/api/v1/badges/f0366300-89bf-4020-a3c1-18b67f8fc21c/deploy-status)](https://app.netlify.com/sites/esai-pomodoro-test/deploys)

This project provides a simple Pomodoro timer. Nothing more - nothing less. The purpose of it is mainly, to start a project, and bring it to a certain point, where I can release it as 1.0, which happened if you read this. :tada:

As this is mainly a learning project right now, please feel free to give hints for possible improvements. Thank you :)

## Features

- Start a Pomodoro cycle ((4 x (25, 5)), 15)
- Pause
- Skip a "lap"

Try it on [netlify](https://netlify.com): [link](https://esai-pomodoro.netlify.com/)

Read more about pomodoro: [link](https://en.wikipedia.org/wiki/Pomodoro_Technique)

Similar software: [link](https://electronjs.org/apps/pomotroid)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run unit tests
```
npm run test:unit
```
