
module.exports = {
  root: true,
  env: {
    [process.env.IS_TESTING ? 'jest' : 'node']: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript',
    'plugin:jsx-a11y/recommended',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'class-methods-use-this': ['error', { exceptMethods: ['render', 'mounted', 'updated', 'beforeUpdate'] }],
    'function-paren-newline': 'off',
    'implicit-arrow-linebreak': 'off',
    'no-plusplus': 'off',
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': ['error', { devDependencies: true, optionalDependencies: false, peerDependencies: true }],
  },
  plugins: [
    'jsx-a11y',
  ],
  parserOptions: {
    parser: 'typescript-eslint-parser',
  },
};
