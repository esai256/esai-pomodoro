import Vue from 'vue';

import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

import App from './App';
import router from './router';
import store from './store';
import './registerServiceWorker';

Sentry.init({
  dsn: 'https://007187ad72784a97903f0eacafa0faa4@sentry.io/1427441',
  integrations: [new Integrations.Vue({ Vue, attachProps: true })],
});

Vue.config.productionTip = false;

// Let's check if the browser supports notifications
if (!('Notification' in window)) {
  /* eslint-disable-next-line no-console */
  console.warn('This browser does not support desktop notification');
} else if (Notification && Notification.permission === 'default') {
  Notification.requestPermission();
}

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
