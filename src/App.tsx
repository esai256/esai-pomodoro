import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import ThreeRowLayout from '@/components/three-row-layout';
import CenteredLayout from './components/centered-layout';
import './main.css';

@Component
export default class AppRoot extends Vue {
  render() {
    return (
      <main class="app">
        <ThreeRowLayout>

          <CenteredLayout>
            <router-view />
          </CenteredLayout>

        </ThreeRowLayout>
      </main>
    );
  }
}
