import { mount } from '@vue/test-utils';
import TachometerCountdown from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(TachometerCountdown);

    expect(wrapper).toMatchSnapshot();
  });
});
