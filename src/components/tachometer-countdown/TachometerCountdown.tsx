// import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import Countdown from '../countdown';
/* eslint-disable camelcase */
import css_tachometerCountdown from './tachometer-countdown.module.css';
/* eslint-enable camelcase */

@Component
export default class TachometerCountdown extends Countdown {
  render() {
    return (
      <div class={css_tachometerCountdown.tachometerCountdown}>
        <div
          class={css_tachometerCountdown.tachometerCountdownNeedle}
          style={{
            transform: `rotate(${180 + (180 / (this.duration / this.timeLeft))}deg) translateX(100px)`,
            transformOrigin: 'top center',
          }}
        >
        </div>
      </div>
    );
  }
}
