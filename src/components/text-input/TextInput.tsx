import Vue from 'vue';
import { Component, Prop, Inject } from 'vue-property-decorator';
import FormFieldInput from '../form-field/FormFieldInput';

/* eslint-disable camelcase */
import css_textInput from './text-input.module.css';
/* eslint-enable camelcase */

@Component
export default class TextInput extends Vue implements FormFieldInput<string> {
  _tsxattrs: any;

  @Inject()
  formFieldId?: string;

  @Prop()
  value?: string;

  render() {
    return (
      <input
        class={css_textInput.textInput}
        id={this.formFieldId}
        type="text"
        value={this.value}
      />
    );
  }
}
