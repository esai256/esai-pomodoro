import { mount } from '@vue/test-utils';
import TextInput from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(TextInput, {
      provide: { formFieldId: 0 },
    });

    expect(wrapper).toMatchSnapshot();
  });
});

// no further behavior testing needed, as it is just a styled native input
