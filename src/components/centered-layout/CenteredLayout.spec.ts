import { mount } from '@vue/test-utils';
import CenteredLayout from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(CenteredLayout);

    expect(wrapper).toMatchSnapshot();
  });
});
