import Vue from 'vue';
import { Component } from 'vue-property-decorator';

/* eslint-disable camelcase */
import css_centeredLayout from './centered-layout.module.css';
/* eslint-enable camelcase */

@Component
export default class CenteredLayout extends Vue {
  render() {
    return (
      <div class={css_centeredLayout.centeredLayout}>
        {this.$slots.default}
      </div>
    );
  }
}
