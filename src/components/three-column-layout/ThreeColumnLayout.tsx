import { Component } from 'vue-property-decorator';
import Vue from 'vue';

/* eslint-disable camelcase */
import css_threeColumnLayout from './three-column-layout.module.css';
/* eslint-enable camelcase */

@Component
export default class ThreeColumnLayout extends Vue {
  render() {
    return (
      <div class={css_threeColumnLayout.threeColumnLayout}>
        <div class={css_threeColumnLayout.threeColumnLayoutLeft}>
          {this.$slots.left}
        </div>
        <div class={css_threeColumnLayout.threeColumnLayoutCenter}>
          {this.$slots.default}
        </div>
        <div class={css_threeColumnLayout.threeColumnLayoutRight}>
          {this.$slots.right}
        </div>
      </div>
    );
  }
}
