import { mount } from '@vue/test-utils';
import ThreeColumnLayout from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(ThreeColumnLayout);

    expect(wrapper).toMatchSnapshot();
  });
});
