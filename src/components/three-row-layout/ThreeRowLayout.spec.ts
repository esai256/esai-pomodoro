import { mount } from '@vue/test-utils';
import ThreeRowLayout from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(ThreeRowLayout);

    expect(wrapper).toMatchSnapshot();
  });
});
