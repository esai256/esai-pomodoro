import { Component } from 'vue-property-decorator';
import Vue from 'vue';

/* eslint-disable camelcase */
import css_threeRowLayout from './three-row-layout.module.css';
/* eslint-enable camelcase */

@Component
export default class ThreeRowLayout extends Vue {
  render() {
    return (
      <div class={css_threeRowLayout.threeRowLayout}>
        <div class={css_threeRowLayout.threeRowLayoutTop}>
          {this.$slots.top}
        </div>
        <div class={css_threeRowLayout.threeRowLayoutMiddle}>
          {this.$slots.default}
        </div>
        <div class={css_threeRowLayout.threeRowLayoutBottom}>
          {this.$slots.bottom}
        </div>
      </div>
    );
  }
}
