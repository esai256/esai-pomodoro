import Vue from 'vue';
import { Component, Prop, Emit } from 'vue-property-decorator';
import classNames from 'classnames';

/* eslint-disable camelcase */
import css_button from './button.module.css';
/* eslint-enable camelcase */

@Component
export default class Button extends Vue {
  _tsxattrs: any;

  @Prop()
  labelText: string | undefined;

  /* eslint-disable class-methods-use-this */
  @Emit('click')
  clickHandler(e: MouseEvent) {
    return e;
  }
  /* eslint-enable class-methods-use-this */

  render() {
    const iconAreaClass = this.$slots.default
      ? css_button.buttonIconAreaWithText
      : css_button.buttonIconAreaOnlyIcon;

    return (
      <button
        type="button"
        class={
          classNames(
            [
              css_button.button,
              {
                [css_button.buttonOnlyIcon]: !this.$slots.default,
              },
            ],
          )
        }
        onClick={this.clickHandler}
      >
        {this.$slots.default}
        <div class={[css_button.buttonIconArea, iconAreaClass]}>
          {this.$slots.iconArea}
        </div>
      </button>
    );
  }
}
