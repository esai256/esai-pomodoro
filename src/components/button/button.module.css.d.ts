export const button: string;
export const buttonOnlyIcon: string;
export const buttonIconArea: string;
export const buttonIconAreaOnlyIcon: string;
export const buttonIconAreaWithText: string;
