import { mount } from '@vue/test-utils';
import Button from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(Button);

    expect(wrapper).toMatchSnapshot();
  });
});

describe('behavior', () => {
  it('should contain passed text', () => {
    const expectedLabelText = 'expectedLabelText';
    const wrapper = mount(Button, {
      slots: {
        default: expectedLabelText,
      },
    });

    expect(wrapper.text()).toMatch(expectedLabelText);
  });
});
