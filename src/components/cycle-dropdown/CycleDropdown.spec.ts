import { mount } from '@vue/test-utils';
import CycleDropdown from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(CycleDropdown, {
      provide: {
        formFieldId: 0,
      },
    });

    expect(wrapper).toMatchSnapshot();
  });
});

describe('behavior', () => {
  it('should extract the label correctly', () => {
    const expectedLabel = 'expectedLabel';
    const wrapper = mount(CycleDropdown, {
      provide: {
        formFieldId: 0,
      },
      data: () => ({
        isOpen: true,
      }),
      propsData: {
        items: [{
          name: expectedLabel,
          laps: [],
          repititions: 0,
        }],
      },
    });

    expect(wrapper.find('[role="option"]').text()).toMatch(expectedLabel);
  });
});
