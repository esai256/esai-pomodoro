import { Component, Prop } from 'vue-property-decorator';
import Dropdown from '../dropdown';

@Component
export default class CycleDropdown extends Dropdown<TCycle> {
  @Prop({
    default: () => (element: TCycle) => element.name || 'unknown',
  })
  getLabelFromItem!: ((element: TCycle) => string);
}
