import Vue from 'vue';
import { Component, Prop, Emit } from 'vue-property-decorator';
import DropdownItem from './DropdownItem';

/* eslint-disable camelcase */
import css_dropdownItemList from './dropdown__item-list.module.css';
/* eslint-enable camelcase */

@Component
export default class DropdownItemList extends Vue {
  _tsxattrs: any;

  get defaultedItems() {
    return this.items || [];
  }

  @Prop({ default: [] })
  items: string[] | undefined;

  @Prop({ default: null })
  selectedItemKey!: number | null;

  /* eslint-disable class-methods-use-this */
  @Emit('select')
  selectHandler(itemKey: number) {
    return itemKey;
  }
  /* eslint-enable class-methods-use-this */

  render() {
    return (
      <div role="listbox" class={css_dropdownItemList.dropdownItemList}>
        {this.defaultedItems.map((label, key) =>
          <DropdownItem
            isSeleced={key === this.selectedItemKey}
            onClick={() => this.selectHandler(key)}
          >
            {label}
          </DropdownItem>,
        )}
      </div>
    );
  }
}
