export const dropdown: string;
export const dropdownOpened: string;
export const dropdownClosed: string;
export const dropdownIcon: string;
export const dropdownIconOpened: string;
export const dropdownIconClosed: string;
