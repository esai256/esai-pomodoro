import Vue from 'vue';
import { Component, Emit, Prop } from 'vue-property-decorator';

/* eslint-disable camelcase */
import css_dropdownItem from './dropdown__item.module.css';
/* eslint-enable camelcase */

@Component
export default class DropdownItem extends Vue {
  _tsxattrs: any;

  @Prop({ default: false })
  isSelected!: boolean;

  /* eslint-disable class-methods-use-this */
  @Emit('click')
  clickHandler(e: MouseEvent) {
    return e;
  }
  /* eslint-enable class-methods-use-this */

  keypressHandler(e:KeyboardEvent) {
    const ENTER_KEYCODE = 13;

    if (e.keyCode === ENTER_KEYCODE) {
      (this.$el as HTMLElement).click();
    }
  }

  render() {
    return (
      <div
        class={css_dropdownItem.dropdownItem}
        onClick={this.clickHandler}
        tabindex={0}
        onKeypress={e => this.keypressHandler(e)}
        role="option"
        aria-selected={this.isSelected}
      >
        {this.$slots.default}
      </div>
    );
  }
}
