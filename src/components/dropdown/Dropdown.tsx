import Vue from 'vue';
import {
  Component,
  Prop,
  Emit,
  Inject,
} from 'vue-property-decorator';
import Button from '../button';
import DropdownItemList from './DropdownItemList';
import IconChevron from '../icons/IconChevron';

/* eslint-disable camelcase */
import css_dropdown from './dropdown.module.css';
import FormFieldInput from '../form-field/FormFieldInput';
/* eslint-enable camelcase */

@Component
export default class Dropdown<T=string> extends Vue implements FormFieldInput<T> {
  _tsxattrs: any;

  isOpen: boolean = false;

  @Inject()
  formFieldId?: string;

  @Prop({
    default: () => ({
      name: 'Please choose...',
      repititions: 0,
      laps: [],
    }),
  })
  blankItem!: T;

  @Prop() value?: T;

  @Prop({ default: () => [] })
  items!: T[];

  @Prop({ default: () => (element: T) => String(element || '') })
  getLabelFromItem!: ((element: T) => string);

  get defaultedItems() {
    return this.items || [];
  }

  get defaultedGetLabelFromItem() {
    return this.getLabelFromItem || (() => '');
  }

  get valueLabel() {
    return this.defaultedGetLabelFromItem(this.value || this.blankItem);
  }

  get labelList() {
    return this.defaultedItems.map(item => this.defaultedGetLabelFromItem(item));
  }

  get selectedItemKey() {
    return this.defaultedItems.findIndex(item => item === this.value);
  }

  @Emit('toggle')
  clickHandler() {
    this.isOpen = !this.isOpen;

    return this.isOpen;
  }

  @Emit('select')
  selectHandler(itemKey: number) {
    this.isOpen = false;
    return this.defaultedItems[itemKey];
  }

  render() {
    return (
      <div
        class={[
          css_dropdown.dropdown,
          this.isOpen
            ? css_dropdown.dropdownOpened
            : css_dropdown.dropdownClosed,
        ]}
      >
        <Button
          id={this.formFieldId}
          onClick={this.clickHandler}
        >
          {this.valueLabel}
          <IconChevron
            class={[
              css_dropdown.dropdownIcon,
              this.isOpen
                ? css_dropdown.dropdownIconOpened
                : css_dropdown.dropdownIconClosed,
            ]}
            slot="iconArea"
          />
        </Button>
        {this.isOpen
          && <DropdownItemList
            selectedItemKey={this.selectedItemKey}
            items={this.labelList}
            getLabelFromElement={this.getLabelFromItem}
            onSelect={this.selectHandler}
          />
        }
      </div>
    );
  }
}
