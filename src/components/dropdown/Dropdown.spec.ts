import { mount, shallowMount } from '@vue/test-utils';
import Dropdown from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 0 },
    });

    expect(wrapper).toMatchSnapshot();
  });
});

describe('behavior', () => {
  it('should open on click', () => {
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 'formfieldid' },
    });

    wrapper.find('#formfieldid').trigger('click');

    expect(wrapper.find('[role="listbox"]').exists()).toBe(true);
  });
  it('should list all given items', () => {
    const expectedItems = ['expectedItem1', 'expectedItem2'];
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 'formfieldid' },
      propsData: {
        items: expectedItems,
      },
    });

    wrapper.find('#formfieldid').trigger('click');

    const actualResult = wrapper.findAll('[role="option"]').wrappers.map(option => option.text());

    expect(actualResult).toEqual(expectedItems);
  });
  it('should trigger an selected event when item is selected', () => {
    const expectedItem = 'expectedItem';
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 'formfieldid' },
      propsData: {
        items: [expectedItem],
      },
    });

    wrapper.find('#formfieldid').trigger('click');
    wrapper.find('[role="option"]').trigger('click');
    expect(wrapper.emitted('select').length).toBe(1);
    expect(wrapper.emitted('select')[0][0]).toBe(expectedItem);
  });
  it('should be usable by keyboard', () => {
    const expectedItem = 'expectedItem';
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 'formfieldid' },
      propsData: {
        items: [expectedItem],
      },
    });

    // click is enough, as the native button element is used which is accessible
    // (and simulating doesn't work for some reason)
    wrapper.find('#formfieldid').trigger('click');
    wrapper.find('[role="option"]').trigger('keypress.enter');
    expect(wrapper.emitted('select').length).toBe(1);
    expect(wrapper.emitted('select')[0][0]).toBe(expectedItem);
  });
  it('should close itself once a value was selected', () => {
    const expectedItem = 'expectedItem';
    const wrapper = mount(Dropdown as any, {
      provide: { formFieldId: 'formfieldid' },
      propsData: {
        items: [expectedItem],
      },
    });

    wrapper.find('#formfieldid').trigger('click');
    wrapper.find('[role="option"]').trigger('click');
    expect(wrapper.find('[role="listbox"]').exists()).toBe(false);
  });
});
