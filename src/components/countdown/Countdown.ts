import Vue from 'vue';
import { Prop } from 'vue-property-decorator';

export default abstract class Countdown extends Vue {
  _tsxattrs: any;

  @Prop({ default: 0 })
  duration!: number;

  @Prop({ default: 0 })
  timeLeft!: number;

  abstract render(): JSX.Element;
}
