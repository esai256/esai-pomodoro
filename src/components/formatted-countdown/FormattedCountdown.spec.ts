import { mount } from '@vue/test-utils';
import FormattedCountdown from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(FormattedCountdown, {
      propsData: {
        duration: 63766861001,
        timeLeft: 63766861001,
      },
    });

    expect(wrapper).toMatchSnapshot();
  });
});
