import getFormattedTime from '@/logic/getFormattedTime';
import { Component, Prop, Emit } from 'vue-property-decorator';
import Countdown from '../countdown';

/* eslint-disable camelcase */
import css_formattedCountdown from './formatted-countdown.module.css';
/* eslint-enable camelcase */

@Component
export default class FormattedCountdown extends Countdown {
  _tsxattrs: any;

  @Prop({ default: 0 })
  duration!: number;

  @Prop({ default: 0 })
  timeLeft!: number;

  render() {
    return (
      <div class={css_formattedCountdown.formattedCountdown}>
        {getFormattedTime(this.timeLeft)}
      </div>
    );
  }
}
