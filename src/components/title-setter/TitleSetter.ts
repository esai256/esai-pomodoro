import getFormattedTime from '@/logic/getFormattedTime';
import Vue from 'vue';
import { Component, Prop, Watch } from 'vue-property-decorator';

@Component
class TitleSetter extends Vue {
  _tsxattrs: any;

  private initialTitle: string = document.title;

  @Prop({ default: null })
  public timeLeft!: number | null;

  @Prop({ default: null })
  public lapName!: string | null;

  @Watch('timeLeft')
  @Watch('lapName')
  public watcher() {
    this.updateTitle();
  }

  private updateTitle() {
    if (this.lapName && this.timeLeft) {
      document.title = `[${this.lapName} ${getFormattedTime(this.timeLeft)}] ${this.initialTitle}`;
    } else {
      document.title = this.initialTitle;
    }
  }

  public mounted() {
    this.updateTitle();
  }

  public render() {
    return null;
  }
}

export default TitleSetter;
