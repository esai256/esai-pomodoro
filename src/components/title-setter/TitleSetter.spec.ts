import { mount } from '@vue/test-utils';
import TitleSetter from '.';

let preservedTitle: string;

describe('behavior', () => {
  beforeAll(() => {
    preservedTitle = document.title;
  });

  beforeEach(() => {
    document.title = 'TEST';
  });

  afterAll(() => {
    document.title = preservedTitle;
  });

  it('should match the snapshot when values have been given', () => {
    mount(TitleSetter, {
      propsData: {
        lapName: 'Focus',
        timeLeft: 15000,
      },
    });

    expect(document.title).toMatchInlineSnapshot('"[Focus 15s] TEST"');
  });

  it('should match the snapshot when values have been updated after mount', () => {
    const wrapper = mount(TitleSetter, {
      propsData: {
        lapName: 'Focus',
        timeLeft: 15000,
      },
    });

    wrapper.setProps({
      lapName: 'Break',
      timeLeft: 39000,
    });

    expect(document.title).toMatchInlineSnapshot('"[Break 39s] TEST"');
  });

  it('should not change the title if one value is missing', () => {
    const expectedUnchangedTitle = document.title;
    mount(TitleSetter, {
      propsData: {
        lapName: null,
        timeLeft: 15000,
      },
    });

    expect(document.title).toBe(expectedUnchangedTitle);
  });

  it('should return to the initial title, if one value goes missing', () => {
    const expectedUnchangedTitle = document.title;
    const wrapper = mount(TitleSetter, {
      propsData: {
        lapName: 'focus',
        timeLeft: 15000,
      },
    });

    wrapper.setProps({
      lapName: null,
      timeLeft: 12,
    });

    expect(document.title).toBe(expectedUnchangedTitle);
  });
});
