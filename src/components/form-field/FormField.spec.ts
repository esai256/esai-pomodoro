import { mount } from '@vue/test-utils';
import FormField from '.';

describe('appearance', () => {
  it('should match the snapshot', () => {
    const wrapper = mount(FormField);

    expect(wrapper).toMatchSnapshot();
  });
});

describe('behavior', () => {
  it('should display the given label', () => {
    const expectedLabelText = 'expectedLabelText';
    const wrapper = mount(FormField, {
      propsData: {
        label: expectedLabelText,
      },
    });

    expect(wrapper.find('label').text()).toEqual(expectedLabelText);
  });
  it('should not render a label if no label text was given', () => {
    const wrapper = mount(FormField);

    expect(wrapper.find('label').exists()).toBe(false);
  });
  it('should render the default slot', () => {
    const defaultSlot = 'defaultSlot';
    const wrapper = mount(FormField, {
      slots: {
        default: defaultSlot,
      },
    });

    expect(wrapper.html().includes(defaultSlot)).toBe(true);
  });

  // TODO
  // Here should be a test, for provisioning of label link id
  // I couldn't find the right way to do it with vue-test-utils though
});
