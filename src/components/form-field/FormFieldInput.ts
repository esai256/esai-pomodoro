interface FormFieldInput<T> {
  formFieldId?: string;
  value?: T;
}

export default FormFieldInput;
