import Vue from 'vue';
import { Component, Prop, Provide } from 'vue-property-decorator';

/* eslint-disable camelcase */
import css_formField from './form-field.module.css';
/* eslint-enable camelcase */

@Component
export default class FormField extends Vue {
  _tsxattrs: any;

  @Prop()
  label?: string;

  @Provide()
  formFieldId = `formField-${Math.ceil(Math.random() * 100)}`;

  render() {
    return (
      <div class={css_formField.formField}>
        {this.label
          && <label class={css_formField.formFieldLabel} htmlfor={this.formFieldId}>
            {this.label}
          </label>
        }
        {this.$slots.default}
      </div>
    );
  }
}
