import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class IconChevron extends Vue {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100"><g><g transform="translate(50 50) scale(0.69 0.69) rotate(90) translate(-50 -50)" style="fill:#000000"><svg fill="#000000" xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;"><path d="M28.8,94.4c-1.3,0-2.6-0.5-3.6-1.5c-2-2-2-5.2,0-7.2l35.5-35.5L25.2,14.8c-2-2-2-5.2,0-7.2s5.2-2,7.2,0l38.9,38.9  c0.9,0.9,1.5,2.2,1.5,3.6c0,1.3-0.5,2.9-1.5,3.8L32.4,92.9C31.4,93.9,30.1,94.4,28.8,94.4z"></path></svg></g></g></svg >
    );
  }
}
