import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class IconRestart extends Vue {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" style="enable-background:new 0 0 100 100;"><path d="M54.5,10.6c-16.8,0-31.6,10.6-37.3,25.9l-2.7-3.8c-1.6-2.2-4.7-2.7-7-1.1c-2.2,1.6-2.7,4.7-1.1,7l8.9,12.2  c1,1.3,2.5,2.1,4,2.1c0.7,0,1.3-0.1,2-0.4l14-6c2.5-1.1,3.7-4,2.6-6.5c-1.1-2.5-4-3.7-6.5-2.6l-4.5,1.9  c4.5-11.1,15.4-18.7,27.7-18.7C71,20.6,84.4,34,84.4,50.5C84.4,67,71,80.4,54.5,80.4c-8.7,0-17-3.8-22.7-10.4  c-1.8-2.1-4.9-2.3-7-0.5c-2.1,1.8-2.3,4.9-0.5,7c7.6,8.9,18.6,13.9,30.3,13.9c22,0,39.9-17.9,39.9-39.9  C94.4,28.5,76.5,10.6,54.5,10.6z"></path></svg>
    );
  }
}
