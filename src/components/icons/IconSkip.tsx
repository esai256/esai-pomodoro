import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class Chevron extends Vue {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><g><path d="M21,5v14c0,0.55-0.45,1-1,1h-3c-0.55,0-1-0.45-1-1V5c0-0.55,0.45-1,1-1h3C20.55,4,21,4.45,21,5z"></path><path d="M15,12c0,0.33-0.16,0.63-0.43,0.82l-10,7C4.4,19.94,4.2,20,4,20c-0.16,0-0.32-0.04-0.46-0.11C3.21,19.71,3,19.37,3,19V5    c0-0.37,0.21-0.71,0.54-0.89c0.33-0.17,0.73-0.14,1.03,0.07l10,7C14.84,11.37,15,11.67,15,12z"></path></g></g></svg>
    );
  }
}
