import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class IconPlay extends Vue {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><path d="M19.54,11.16l-14-9c-0.31-0.2-0.7-0.21-1.02-0.04C4.2,2.3,4,2.63,4,3v18c0,0.37,0.2,0.7,0.52,0.88C4.67,21.96,4.84,22,5,22   c0.19,0,0.38-0.05,0.54-0.16l14-9C19.83,12.66,20,12.34,20,12C20,11.66,19.83,11.34,19.54,11.16z"></path></g></svg>
    );
  }
}
