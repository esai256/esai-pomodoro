import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class IconPause extends Vue {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" version="1.1" x="0px" y="0px" viewBox="0 0 24 24" enable-background="new 0 0 24 24"><g><g><path d="M9,2H6C4.9,2,4,2.9,4,4v16c0,1.1,0.9,2,2,2h3c1.1,0,2-0.9,2-2V4C11,2.9,10.1,2,9,2z"></path><path d="M18,2h-3c-1.1,0-2,0.9-2,2v16c0,1.1,0.9,2,2,2h3c1.1,0,2-0.9,2-2V4C20,2.9,19.1,2,18,2z"></path></g></g></svg>
    );
  }
}
