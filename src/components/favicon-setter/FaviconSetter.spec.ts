import { mount } from '@vue/test-utils';
import FaviconSetter from '.';
import { Favicon } from './FaviconSetter';

describe('behavior', () => {
  const linkElementMock = {
    setAttribute: jest.fn(),
  } as unknown as HTMLElement;
  jest.spyOn(document, 'getElementById').mockImplementation(() => linkElementMock);

  it('change the favicon once the prop changes', () => {
    const wrapper = mount(FaviconSetter, {
      propsData: {
        favicon: Favicon.default,
      },
    });

    wrapper.setProps({ favicon: Favicon.play });

    expect(linkElementMock.setAttribute).toHaveBeenCalledWith('href', 'favicon-play.ico');
  });
});
