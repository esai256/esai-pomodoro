import Vue from 'vue';
import { Component, Prop, Watch } from 'vue-property-decorator';

export enum Favicon {
  default = 'default',
  play = 'play'
}

const faviconMapper = {
  [Favicon.default]: 'favicon.ico',
  [Favicon.play]: 'favicon-play.ico',
};

@Component
class FaviconSetter extends Vue {
  _tsxattrs: any;

  @Prop()
  public favicon!: Favicon;

  @Watch('favicon')
  public faviconWatcher() {
    const faviconElement = document.getElementById('favicon');

    if (faviconElement) {
      faviconElement.setAttribute('href', faviconMapper[this.favicon]);
    }
  }

  public render() {
    return null;
  }
}

export default FaviconSetter;
