import Vue from 'vue';
import Router from 'vue-router';
import ActiveView from './views/Active';
import ComponentsView from './views/Components';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: ActiveView,
    },
    {
      path: '/components',
      name: 'Components',
      component: ComponentsView,
    },
  ],
});
