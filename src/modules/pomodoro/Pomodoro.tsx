import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';
import { mapGetters, mapActions } from 'vuex';
import Button from '@/components/button';
import FormattedCountdown from '@/components/formatted-countdown';
import FormField from '@/components/form-field';
import IconPause from '@/components/icons/IconPause';
import IconPlay from '@/components/icons/IconPlay';
import IconSkip from '@/components/icons/IconSkip';
import IconStop from '@/components/icons/IconStop';
import IconRestart from '@/components/icons/IconRestart';
import FaviconSetter, { Favicon } from '@/components/favicon-setter/FaviconSetter';
import TitleSetter from '@/components/title-setter/TitleSetter';

/* eslint-disable camelcase */
import css_pomodoro from './pomodoro.module.css';
/* eslint-enable camelcase */

@Component({
  computed: {
    ...mapGetters([
      'chosenCycle',
      'generator',
      'hasStarted',
      'hasPaused',
      'currentLap',
      'currentTimeLeft',
    ]),
  },
  methods: {
    ...mapActions([
      'resetGenerator',
      'startLap',
      'pauseLap',
      'restartLap',
      'skipLap',
      'resetCycle',
    ]),
  },
})
export default class Pomodoro extends Vue {
  _tsxattrs: any;

  chosenCycle!: TCycle | null;

  generator!: Generator | null;

  currentTimeLeft!: number;

  resetGenerator!: () => void;

  currentLap!: TLap | null;

  @Prop({
    default: false,
  })
  startOnMount!: boolean;

  hasStarted!: boolean;

  hasPaused!: boolean;

  startLap!: () => void;

  restartLap!: () => void;

  resetCycle!: () => void;

  skipLap!: () => void;

  pauseLap!: () => void;

  mounted() {
    if (this.startOnMount) {
      this.startLap();
    }
  }

  get isRunning() {
    return this.hasStarted && !this.hasPaused;
  }

  render() {
    return (
      <div class={css_pomodoro.pomodoro}>
        <FaviconSetter favicon={this.isRunning ? Favicon.play : Favicon.default} />

        <TitleSetter
          lapName={this.currentLap && this.currentLap.name}
          timeLeft={this.currentTimeLeft}
        />

        {this.currentLap && [
            <FormField label={this.currentLap.name}>
              <FormattedCountdown
                duration={this.currentLap!.duration}
                timeLeft={this.currentTimeLeft}
              />
            </FormField>,
            <FormField>
              <Button onClick={this.hasPaused ? this.startLap : this.pauseLap}>
                {this.hasPaused ? 'Continue' : 'Pause'}
                {
                  this.hasPaused
                    ? <IconPlay slot="iconArea" />
                    : <IconPause slot="iconArea" />
                }
              </Button>
            </FormField>,
            <FormField>
              <Button onClick={this.skipLap}>
                Skip
                <IconSkip slot="iconArea" />
              </Button>
            </FormField>,
            <FormField>
              <Button onClick={this.restartLap}>
                Restart lap
              <IconRestart slot="iconArea" />
              </Button>
            </FormField>,
        ]}
        {this.hasStarted
          ? <FormField>
            <Button onClick={this.resetCycle}>
              Reset
            <IconStop slot="iconArea" />
            </Button>
          </FormField>
          : <FormField>
            <Button onClick={this.startLap}>
              Start
            <IconPlay slot="iconArea" />
            </Button>
          </FormField>
        }
      </div>
    );
  }
}
