import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';
import FormField from '@/components/form-field';
import CycleDropdown from '@/components/cycle-dropdown';

@Component
export default class CycleChoice extends Vue {
  @Prop({
    default: () => [],
  })
  cycles!: TCycle[];

  chosenCycle: TCycle | null = null;

  render() {
    return (
      <FormField label="Choose your cycle">
        <CycleDropdown
          items={this.cycles}
          value={this.chosenCycle}
          onSelect={(item: TCycle) => {
            this.chosenCycle = item;
          }}
        />
      </FormField>
    );
  }
}
