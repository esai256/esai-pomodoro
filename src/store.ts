/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex, { Store, Dictionary } from 'vuex';
import lapGenerator from './logic/lapGenerator';
import notificationSound from './notificationSound';

Vue.use(Vuex);

export type TPomodoroState = {
  chosenCycle: TCycle | null,
  generator: Generator | null,
  currentIntervalId: number | null,
  currentLap: TLap | null,
  currentTimeLeft: number,
};

const CLASSIC_CYCLE = {
  name: 'classic',
  repititions: 1,
  laps: [
    {
      repititions: 3,
      laps: [
        { duration: 1500000, name: 'focus' },
        { duration: 300000, name: 'short break' },
      ] as TLap[],
    } as TCycle,
    { duration: 1500000, name: 'focus' } as TLap,
    { duration: 900000, name: 'long break' } as TLap,
  ] as (TLap | TCycle)[],
};

export const mutations = {
  setGenerator(state: {
    chosenCycle: TCycle | null;
    generator: IterableIterator<TLap> | null;
  }, cycle: TCycle) {
    state.generator = lapGenerator(cycle);
  },
  setCurrentIntervalId(state: any, currentIntervalId: number | null) {
    state.currentIntervalId = currentIntervalId;
  },
  setCurrentTimeLeft(state: any, currentTimeLeft: number) {
    state.currentTimeLeft = currentTimeLeft;
  },
  setCurrentLap(state: any, currentLap: TLap | null) {
    state.currentLap = currentLap;
  },
};

export const actions = {
  resetGenerator({ commit, state }: Store<TPomodoroState>) {
    commit('setGenerator', state.chosenCycle);
  },
  startLap({ commit, state, dispatch }: Store<TPomodoroState>, restart = false) {
    if (state.generator) {
      if (!state.currentLap || (state.currentTimeLeft <= 0 && !restart)) {
        const { value, done } = state.generator.next();

        if (done) {
          dispatch('pause');
          dispatch('resetGenerator');
          dispatch('startLap');

          return;
        }

        const hasLapRegularlyEnded = Boolean(state.currentLap) && state.currentTimeLeft <= 0;

        commit('setCurrentLap', value);

        // don't notify on manual skip
        if (hasLapRegularlyEnded) {
          notificationSound.play();
          if (!('Notification' in window)) {
          /* eslint-disable-next-line no-new */
            new Notification(
              'esai-pomodoro',
              {
                icon: '/img/icons/android-chrome-192x192.png',
                body: `Now starting: ${state.currentLap!.name}`,
              },
            ); // TODO: localization & Notification Abstraction
          }
        }
      }

      commit('setCurrentTimeLeft', state.currentTimeLeft || state.currentLap!.duration);

      const startTime = Date.now();
      const durationToUse = state.currentTimeLeft;

      commit('setCurrentIntervalId', setInterval(() => {
        const timeLeft = durationToUse - (Date.now() - startTime);

        commit('setCurrentTimeLeft', timeLeft <= 0 ? 0 : timeLeft);
        if (!state.currentTimeLeft) {
          clearInterval(state.currentIntervalId!);
          dispatch('startLap');
        }
      }, 1000));
    }
  },
  restartLap({ commit, dispatch, getters }: Store<TPomodoroState>) {
    if (getters.hasStarted) {
      dispatch('pauseLap');
      commit('setCurrentTimeLeft', 0);
      dispatch('startLap', true);
    }
  },
  pauseLap({ commit, state, getters }: Store<TPomodoroState>) {
    if (getters.hasStarted) {
      if (state.currentIntervalId) {
        clearInterval(state.currentIntervalId);
      }

      commit('setCurrentIntervalId', null);
    }
  },
  async skipLap({ commit, state, dispatch }: Store<TPomodoroState>) {
    if (state.generator) {
      await dispatch('pauseLap');
      commit('setCurrentLap', state.generator.next().value);
      commit('setCurrentTimeLeft', state.currentLap ? state.currentLap.duration : 0);
      dispatch('startLap');
    }
  },
  async resetCycle({ commit, getters, dispatch }: Store<TPomodoroState>) {
    if (getters.hasStarted) {
      await dispatch('pauseLap');
      dispatch('resetGenerator');
      commit('setCurrentLap', null);
      commit('setCurrentTimeLeft', 0);
    }
  },
};

export const getters: any = {
  chosenCycle({ chosenCycle }: TPomodoroState) {
    return chosenCycle;
  },
  generator({ generator }: TPomodoroState) {
    return generator;
  },
  hasStarted({ currentLap }: TPomodoroState) { return Boolean(currentLap); },
  hasPaused(
    { currentIntervalId }:
      TPomodoroState,
    currentGetters: Dictionary<any>,
  ) {
    return currentGetters.hasStarted && !currentIntervalId;
  },
  currentTimeLeft({ currentTimeLeft }: TPomodoroState) {
    return currentTimeLeft;
  },
  currentLap({ currentLap }: TPomodoroState) {
    return currentLap;
  },
};

export default new Vuex.Store({
  state: {
    // FIXME
    chosenCycle: CLASSIC_CYCLE,
    generator: lapGenerator(CLASSIC_CYCLE),
    currentIntervalId: null,
    currentLap: null,
    currentTimeLeft: 0,
  } as TPomodoroState,
  mutations,
  actions,
  getters,
} as any);
