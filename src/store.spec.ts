/* eslint-disable require-yield, no-empty-function */
import { Store } from 'vuex';
import {
  getters, mutations, actions, TPomodoroState,
} from './store';

jest.useFakeTimers();

describe('store', () => {
  describe('getters', () => {
    it('should get the current cycle', () => {
      const expectedValue = {
        name: 'expectedCycle',
        repititions: 1,
        laps: [],
      };

      const actualValue = getters.chosenCycle({ chosenCycle: expectedValue });

      expect(actualValue).toEqual(expectedValue);
    });

    it('should return the current value of `generator` of the state', () => {
      const expectedValue = {};
      const actualValue = getters.generator({ generator: expectedValue as any });

      expect(actualValue).toEqual(expectedValue);
    });
  });

  describe('mutations', () => {
    it('should set the current generator', () => {
      const fakeGenerator = function* fakeGenerator(obj: any): any { yield obj; };
      const oldGen = fakeGenerator({});
      const stateToMutate = {
        chosenCycle: {} as TCycle,
        generator: oldGen,
      };
      const newObject = {} as TCycle;

      expect(stateToMutate.generator.next().value).not.toBe(newObject);
      mutations.setGenerator(stateToMutate, newObject);
      expect(stateToMutate.generator).not.toBe(oldGen);
      expect(typeof stateToMutate.generator.next).toBe('function');
    });

    it('should set the current interval id', () => {
      const EXPECTED_INTERVAL_ID = 1;
      const stateToMutate = {
        currentIntervalId: 0,
      };

      mutations.setCurrentIntervalId(stateToMutate, EXPECTED_INTERVAL_ID);
      expect(stateToMutate.currentIntervalId).toBe(EXPECTED_INTERVAL_ID);
    });

    it('should set the current lap', () => {
      const EXPECTED_LAP = {} as TLap;
      const stateToMutate = {
        currentLap: null,
      };

      mutations.setCurrentLap(stateToMutate, EXPECTED_LAP);
      expect(stateToMutate.currentLap).toBe(EXPECTED_LAP);
    });

    it('should set the current time left', () => {
      const EXPECTED_TIME_LEFT = 15;
      const stateToMutate = {
        currentTimeLeft: null,
      };

      mutations.setCurrentTimeLeft(stateToMutate, EXPECTED_TIME_LEFT);
      expect(stateToMutate.currentTimeLeft).toBe(EXPECTED_TIME_LEFT);
    });
  });

  describe('actions', () => {
    it('should reset the current generator', () => {
      const commit = jest.fn() as any;
      const newCycle = {};

      actions.resetGenerator({ commit, state: { chosenCycle: newCycle } } as Store<TPomodoroState>);
      expect(commit).toHaveBeenCalledWith('setGenerator', newCycle);
    });

    it('should start a lap if none is there yet', () => {
      const fakeGenerator = function* fakeGenerator(obj: any) { yield obj; };
      const dispatch = jest.fn() as any;
      const EXPECTED_DURATION = 1000;
      const EXPECTED_FAKE_LAP = { duration: EXPECTED_DURATION };
      const newCycle = {};
      const FAKE_STATE = {
        generator: fakeGenerator(EXPECTED_FAKE_LAP),
        chosenCycle: newCycle,
        currentIntervalId: null,
        currentLap: null,
        currentTimeLeft: 0,
      };
      const commit = jest.fn((key, payload) => {
        switch (key) {
          case 'setCurrentLap':
            FAKE_STATE.currentLap = payload;
            break;
          case 'setCurrentIntervalId':
            FAKE_STATE.currentIntervalId = payload;
            break;
          case 'setCurrentTimeLeft':
            FAKE_STATE.currentTimeLeft = payload;
            break;
          default:
            break;
        }
      }) as any;

      actions.startLap({
        dispatch,
        commit,
        state: FAKE_STATE,
      } as unknown as Store<TPomodoroState>);

      expect(commit).toHaveBeenNthCalledWith(1, 'setCurrentLap', EXPECTED_FAKE_LAP);
      expect(commit).toHaveBeenNthCalledWith(2, 'setCurrentTimeLeft', EXPECTED_DURATION);
      expect(commit).toHaveBeenNthCalledWith(3, 'setCurrentIntervalId', 1);
      expect(setInterval).toHaveBeenCalledTimes(1);
    });

    it('should reset anything and restart cycle once it has ended', () => {
      const fakeGenerator = function* fakeGenerator(): any { } as any;
      const dispatch = jest.fn() as any;
      const newCycle = {};
      const FAKE_STATE = {
        generator: fakeGenerator(),
        chosenCycle: newCycle,
        currentIntervalId: null,
        currentLap: null,
        currentTimeLeft: 0,
      };
      const commit = jest.fn((key, payload) => {
        switch (key) {
          case 'setCurrentLap':
            FAKE_STATE.currentLap = payload;
            break;
          case 'setCurrentIntervalId':
            FAKE_STATE.currentIntervalId = payload;
            break;
          case 'setCurrentTimeLeft':
            FAKE_STATE.currentTimeLeft = payload;
            break;
          default:
            break;
        }
      }) as any;

      actions.startLap({
        dispatch,
        commit,
        state: FAKE_STATE,
      } as unknown as Store<TPomodoroState>);

      expect(dispatch).toHaveBeenNthCalledWith(1, 'pause');
      expect(dispatch).toHaveBeenNthCalledWith(2, 'resetGenerator');
      expect(dispatch).toHaveBeenNthCalledWith(3, 'startLap');
    });

    it('should pause a lap', () => {
      const INTERVAL_ID_TO_CLEAR = 1;
      const commit = jest.fn() as any;
      const newCycle = {};
      const FAKE_STATE = {
        chosenCycle: newCycle,
        currentIntervalId: INTERVAL_ID_TO_CLEAR,
        currentLap: null,
        currentTimeLeft: 0,
      };

      actions.pauseLap({
        commit,
        state: FAKE_STATE,
        getters: { hasStarted: true },
      } as unknown as Store<TPomodoroState>);

      expect(clearInterval).toHaveBeenCalledWith(INTERVAL_ID_TO_CLEAR);
      expect(commit).toHaveBeenCalledWith('setCurrentIntervalId', null);
      expect(commit).toHaveBeenCalledTimes(1);
    });

    it('should skip a lap', async () => {
      const FAKE_LAP = {};
      const fakeGenerator = function* fakeGenerator(): any { return FAKE_LAP; } as any;
      const commit = jest.fn() as any;
      const dispatch = jest.fn().mockResolvedValue(true) as any;
      const newCycle = {};
      const FAKE_STATE = {
        generator: fakeGenerator(),
        chosenCycle: newCycle,
        currentIntervalId: null,
        currentLap: null,
        currentTimeLeft: 0,
      };

      await actions.skipLap({
        commit,
        dispatch,
        state: FAKE_STATE,
        getters: { hasStarted: true },
      } as unknown as Store<TPomodoroState>);

      expect(dispatch).toHaveBeenNthCalledWith(1, 'pauseLap');
      expect(commit).toHaveBeenNthCalledWith(1, 'setCurrentLap', FAKE_LAP);
      expect(commit).toHaveBeenNthCalledWith(2, 'setCurrentTimeLeft', 0);
      expect(dispatch).toHaveBeenNthCalledWith(2, 'startLap');
      expect(dispatch).toHaveBeenCalledTimes(2);
      expect(commit).toHaveBeenCalledTimes(2);
    });

    it('should restart a lap', async () => {
      const FAKE_LAP = {};
      const fakeGenerator = function* fakeGenerator(): any { return FAKE_LAP; } as any;
      const commit = jest.fn() as any;
      const dispatch = jest.fn().mockResolvedValue(true) as any;
      const newCycle = {};
      const FAKE_STATE = {
        generator: fakeGenerator(),
        chosenCycle: newCycle,
        currentIntervalId: null,
        currentLap: null,
        currentTimeLeft: 0,
      };

      await actions.restartLap({
        commit,
        dispatch,
        state: FAKE_STATE,
        getters: { hasStarted: true },
      } as unknown as Store<TPomodoroState>);

      expect(dispatch).toHaveBeenNthCalledWith(1, 'pauseLap');
      expect(commit).toHaveBeenNthCalledWith(1, 'setCurrentTimeLeft', 0);
      expect(dispatch).toHaveBeenNthCalledWith(2, 'startLap', true);
    });

    it('should reset cycle', async () => {
      const FAKE_LAP = {};
      const fakeGenerator = function* fakeGenerator(): any { return FAKE_LAP; } as any;
      const commit = jest.fn() as any;
      const dispatch = jest.fn().mockResolvedValue(true) as any;
      const newCycle = {};
      const FAKE_STATE = {
        generator: fakeGenerator(),
        chosenCycle: newCycle,
        currentIntervalId: null,
        currentLap: null,
        currentTimeLeft: 0,
      };

      await actions.resetCycle({
        commit,
        dispatch,
        state: FAKE_STATE,
        getters: { hasStarted: true },
      } as unknown as Store<TPomodoroState>);

      expect(dispatch).toHaveBeenNthCalledWith(1, 'pauseLap');
      expect(dispatch).toHaveBeenNthCalledWith(2, 'resetGenerator');
      expect(commit).toHaveBeenNthCalledWith(1, 'setCurrentLap', null);
      expect(commit).toHaveBeenNthCalledWith(2, 'setCurrentTimeLeft', 0);
    });
  });
});
