/* eslint-disable no-restricted-syntax */

function isLap(element: TLap | TCycle): element is TLap {
  return (<TLap>element).duration !== undefined;
}

function* lapGenerator(cycle: TCycle): IterableIterator<TLap> {
  for (let i = 0; i < cycle.repititions; i++) {
    for (const lap of cycle.laps) {
      if (isLap(lap)) {
        yield lap;
      } else {
        const gen = lapGenerator(lap);

        for (; ;) {
          const { done, value } = gen.next();

          if (done) {
            break;
          }

          yield value;
        }
      }
    }
  }
}

export default lapGenerator;
