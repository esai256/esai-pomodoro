import lapGenerator from './lapGenerator';

describe('lap generator', () => {
  it('should match the pattern', () => {
    const expectedResult = [
      '1.1',
      '1.2',
      '1.3.1',
      '1.3.2',
      '1.3.3',
      '1.3.1',
      '1.3.2',
      '1.3.3',
      '1.4',
      '1.5.1',
      '1.5.2',
      '1.5.1',
      '1.5.2',
      '1.5.1',
      '1.5.2',
      '1.6',
    ];
    const actualResult = [];
    const configuration = {
      repititions: 1,
      laps: [
        { name: '1.1', duration: 0 } as TLap,
        { name: '1.2', duration: 0 } as TLap,
        {
          repititions: 2,
          laps: [
            { name: '1.3.1', duration: 0 },
            { name: '1.3.2', duration: 0 },
            { name: '1.3.3', duration: 0 },
          ] as TLap[],
        } as TCycle,
        { name: '1.4', duration: 0 } as TLap,
        {
          repititions: 3,
          laps: [
            { name: '1.5.1', duration: 0 },
            { name: '1.5.2', duration: 0 },
          ] as TLap[],
        } as TCycle,
        { name: '1.6', duration: 0 } as TLap,
      ] as (TLap | TCycle)[],
    } as TCycle;
    const generator = lapGenerator(configuration);

    for (; ;) {
      const { value, done } = generator.next();

      if (done) {
        break;
      } else {
        actualResult.push(value.name);
      }
    }

    expect(actualResult).toEqual(expectedResult);
  });
});
