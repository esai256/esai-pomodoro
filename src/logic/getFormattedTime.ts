const unitArr = [
  {
    milliseconds: 31536000000,
    template: (value: number | string) => `${value} years`,
  },
  {
    milliseconds: 604800000,
    template: (value: number | string) => `${value} weeks`,
  },
  {
    milliseconds: 86400000,
    template: (value: number | string) => `${value} days`,
  },
  {
    milliseconds: 3600000,
    template: (value: number | string) => `${value}h`,
  },
  {
    milliseconds: 60000,
    template: (value: number | string) => `${value}min`,
  },
  {
    milliseconds: 1000,
    template: (value: number | string) => `${value}s`,
    regularRound: true,
    alwaysShow: true,
  },
];

export default function getFormattedTime(timeLeft: number) {
  let rest = timeLeft;

  return unitArr.reduce(
    (
      acc: string[],
      currentVal,
    ) => {
      const value = Math[currentVal.regularRound ? 'round' : 'floor'](rest / currentVal.milliseconds);

      if (value || currentVal.alwaysShow) {
        const valueString = String(value).padStart(2, '0');
        rest -= value * currentVal.milliseconds;
        acc.push(currentVal.template(valueString));
      }

      return acc;
    },
    [],
  ).join(' ');
}
