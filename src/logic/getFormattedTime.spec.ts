import getFormattedTime from './getFormattedTime';

describe('should spread a milisecond value out to hours, minutes, etc', () => {
  it('should match the snapshot for a value above hours level', () => {
    expect(getFormattedTime(12468000)).toMatchInlineSnapshot('"03h 27min 48s"');
  });

  it('should match the snapshot for a value below hours level', () => {
    expect(getFormattedTime(1668000)).toMatchInlineSnapshot('"27min 48s"');
  });

  it('should match the snapshot for a value below minutes level', () => {
    expect(getFormattedTime(15000)).toMatchInlineSnapshot('"15s"');
  });
});
