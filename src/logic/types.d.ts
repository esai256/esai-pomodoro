type TCycle = {
  name?: string
  laps: Array<TCycle | TLap>,
  repititions: number
};

type TLap = {
  name: string
  duration: number
};
