import Vue from 'vue';
import { Component } from 'vue-property-decorator';

@Component
export default class SettingsVue extends Vue {
  render() {
    return <div>Settings</div>;
  }
}
