import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import Pomodoro from '@/modules/pomodoro';

@Component
export default class ActiveView extends Vue {
  render() {
    return <Pomodoro />;
  }
}
