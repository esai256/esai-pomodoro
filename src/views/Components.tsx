import Vue from 'vue';
import { Component } from 'vue-property-decorator';
import Button from '../components/button';
import Dropdown from '../components/dropdown';
import FormField from '../components/form-field';
import TextInput from '../components/text-input';
import CenteredLayout from '@/components/centered-layout';

@Component
export default class ActiveView extends Vue {
  dropdownValue: string | null = null;

  test = false;

  render() {
    return (
      <CenteredLayout>

        <Dropdown
          blankItem="Bitte wählen..."
          items={[
            'Element 1',
            'Element 2',
            'Element 3',
          ]}
          value={this.dropdownValue}
          onSelect={(item: string) => {
            this.dropdownValue = item;
          }}
        />

        <Button onClick={() =>
          /* eslint-disable-next-line no-console */
          console.log('button clicked')
        }>
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/4/42/Love_Heart_SVG.svg"
            slot="iconArea"
            alt="heart"
          />
        </Button>

        <Button onClick={() =>
          /* eslint-disable-next-line no-console */
          console.log('button clicked')
        }>
          Click me
          <img
            src="https://upload.wikimedia.org/wikipedia/commons/4/42/Love_Heart_SVG.svg"
            slot="iconArea"
            alt="heart"
          />
        </Button>

        <FormField label="Dinge">
          <Dropdown
            blankItem="Bitte wählen..."
            items={[
              'Element 1',
              'Element 2',
              'Element 3',
            ]}
            value={this.dropdownValue}
            onSelect={(item: string) => {
              this.dropdownValue = item;
            }}
          >
          </Dropdown>
        </FormField>

        <FormField label="Dinge">
          <TextInput value="Blarg" />
        </FormField>

        {this.test && <div>hurray</div>}
      </CenteredLayout>
    );
  }
}
